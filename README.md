To launch this demo app execute the following commands in your terminal from the say-hi-frontend repo directory:

   * ```npm install```
   * ```npm run start```


   As per the instructions on [say-hi-backend](https://bitbucket.org/blockchainlab/say-hi-backend), be sure to:
   * run testrpc
   * compile and deploy with truffle from the backend folder on your terminal
   * paste the contract's address you get from ```truffle deploy``` onto line 163 of index.html
   ```js 
   contract = web3.eth.contract(sayHi_Abi).at("/** Insert your deployed contract address here **/");
   ```
   * ** Important ** If you modify the contract in any significant way, you'll have to grab the ABI array from the HelloEthereum.json (```say-hi-backend/build/contracts/HelloEthereum.json```)after running truffle compile, and paste it under the variable ```sayHi_Abi``` on line 14 of the index.html file of this repo.